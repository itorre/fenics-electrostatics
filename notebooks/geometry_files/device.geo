SetFactory("OpenCASCADE");

lc = .1;

Point(1) = {0, 0, 0, lc};
Point(2) = {4, 0, 0, lc};
Point(3) = {3, 2, 0, lc/2};
Point(4) = {1, 1, 0, lc/2};
Point(5) = {0.5, 0.5, 0, lc};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,5};
Line(5) = {5,1};

Line Loop(1) = {1,2,3,4,5};
Plane Surface(1) = {1};

Point(6) = {1, 2, 0, lc};
Point(7) = {0, 2, 0, lc};
Point(8) = {0, 1, 0, lc};

Line(6) = {4,6};
Line(7) = {6,7};
Line(8) = {7,8};
Line(9) = {8,5};

Line Loop(2) = {4,6,7,8,9};
Plane Surface(2) = {2};

Point(9) = {2, 1, 0, lc / 5};
Point(10) = {3, 0.5, 0, lc / 5};
Line(10) = {9,10};
Line{10} In Surface {1};

v() = BooleanFragments{Surface{1}; Delete;}{ Surface{2}; Delete; };

Physical Surface('blg', 1) = {1};
Physical Surface('slg', 2) = {2};

Physical Line('source', 1) = {7,8};
Physical Line('drain', 2) = {2};
Physical Line('wall', 3) = {10};