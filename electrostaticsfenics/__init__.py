#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Numerical solver for electrostatic problems uning FeniCS
"""
__version__ = '0.0'
__author__ = 'Iacopo Torre'
from . electrostatics_fenics import *